package com.example.htandroid.constants

object NoImage {
    fun getImageUrl(): String {
        return "https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpgx"
    }
}