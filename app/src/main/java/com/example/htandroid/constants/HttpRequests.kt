package com.example.htandroid.constants

object HttpRequests {

    fun getToken(): String {
        return "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImthdHlhQGdtYWlsLmNvbSIsIm5hbWVpZCI6IjMiLCJuYmYiOjE2NDI1OTM5MzgsImV4cCI6MTY0NjE5MzkzOCwiaWF0IjoxNjQyNTkzOTM4fQ.SNzLq6WjLG5_LIhDtOq6pPL_PCd46pQn0Akzx2e6iRU"
    }

    fun getCurrentUserEmail(): String {
        return "katya@gmail.com"
    }
}