package com.example.htandroid

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.htandroid.adapters.CartItemsAdapter
import com.example.htandroid.data.store.Cart
import kotlinx.android.synthetic.main.activity_cart.*

class CartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        if (Cart.getCartItems().isEmpty()) {
            cartItemsLayout.visibility = View.GONE
        } else {
            emptyText.visibility = View.GONE
            val recyclerView = cartRecyclerView
            val cartItemsAdapter = CartItemsAdapter(Cart.getCartItems(), this)
            recyclerView.adapter = cartItemsAdapter
            recyclerView.layoutManager = LinearLayoutManager(this)

            clearCartButton.setOnClickListener {
                cartItemsLayout.visibility = View.GONE
                emptyText.visibility = View.VISIBLE
                Cart.clearCart()
            }
        }
    }
}