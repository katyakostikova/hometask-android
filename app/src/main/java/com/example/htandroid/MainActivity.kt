package com.example.htandroid

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.loader.app.LoaderManager
import androidx.loader.content.AsyncTaskLoader
import androidx.loader.content.Loader
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.htandroid.adapters.ProductsListAdapter
import com.example.htandroid.constants.HttpRequests
import com.example.htandroid.data.HttpService
import com.example.htandroid.data.model.ProductInList
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Response
import java.io.IOException


class MainActivity : AppCompatActivity(), LoaderManager.LoaderCallbacks<List<ProductInList>> {

    var products: List<ProductInList>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        searchButton.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("filter", searchInput.text.toString())
            LoaderManager.getInstance(this).restartLoader(1, bundle, this)
        }

        addButton.setOnClickListener {
            startActivity(Intent(this, NewProductActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        val bundle = Bundle()
        bundle.putString("filter", "")
        LoaderManager.getInstance(this).initLoader(1, bundle, this);
    }

    fun setProductsList() {
        val recyclerView = productsRecyclerView
        val productsListAdapter = products?.let { ProductsListAdapter(it, this) }
        recyclerView.adapter = productsListAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    //LOADER
    override fun onCreateLoader(id: Int, @Nullable args: Bundle?): Loader<List<ProductInList>?> {
        return ProductsLoader(applicationContext, args)
    }

    override fun onLoadFinished(loader: Loader<List<ProductInList>?>, data: List<ProductInList>?) {
        if (data != null) {
            this.products = data
            setProductsList()
        } else {
            Toast.makeText(this, "Server error", Toast.LENGTH_LONG).show()
        }
    }

    override fun onLoaderReset(loader: Loader<List<ProductInList>?>) {}


    private open class ProductsLoader(context: Context, args: Bundle?) :
        AsyncTaskLoader<List<ProductInList>?>(context) {
        val bundle = args
        @Nullable
        override fun loadInBackground(): List<ProductInList>? {
            var productsList: List<ProductInList>? = null
            val call = bundle?.let {
                HttpService
                    .getProductsApi()
                    ?.getProducts(
                        page = "1",
                        perPage = "40",
                        filter = it.getString("filter",""),
                        token = HttpRequests.getToken()
                    )
            }
            try {
                val res: Response<List<ProductInList>?>? = call?.execute()
                productsList = res?.body()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return productsList
        }

        override fun onStartLoading() {
            super.onStartLoading()
            forceLoad()
        }
    }
}