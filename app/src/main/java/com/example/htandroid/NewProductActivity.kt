package com.example.htandroid

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.loader.app.LoaderManager
import androidx.loader.content.AsyncTaskLoader
import androidx.loader.content.Loader
import com.example.htandroid.constants.HttpRequests
import com.example.htandroid.data.HttpService
import com.example.htandroid.data.model.NewProduct
import com.example.htandroid.data.model.ProductDetails
import retrofit2.Response
import java.io.IOException
import kotlinx.android.synthetic.main.activity_new_product.*

class NewProductActivity : AppCompatActivity(),  LoaderManager.LoaderCallbacks<ProductDetails> {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_product)

        addNewProductButton.setOnClickListener {
            if (titleInput.text.isNotEmpty() && priceInput.text.isNotEmpty() && descInput.text.isNotEmpty()) {
                val bundle = Bundle()
                bundle.putString("title", titleInput.text.toString())
                bundle.putString("price", priceInput.text.toString())
                bundle.putString("desc", descInput.text.toString())
                LoaderManager.getInstance(this).initLoader(1, bundle, this);
            } else {
                Toast.makeText(this, "Enter valid data", Toast.LENGTH_LONG).show()
            }
        }
    }


    override fun onCreateLoader(id: Int, args: Bundle?): AddProductLoader {
        return AddProductLoader(applicationContext, args)
    }

    override fun onLoadFinished(loader: Loader<ProductDetails>, data: ProductDetails?) {
        if (data != null) {
            Toast.makeText(this, "Product has been added", Toast.LENGTH_LONG).show()
            finish()
        } else {
            Toast.makeText(this, "Server error", Toast.LENGTH_LONG).show()
        }
    }

    override fun onLoaderReset(loader: Loader<ProductDetails>) {
    }


    open class AddProductLoader(context: Context, args: Bundle?) :
        AsyncTaskLoader<ProductDetails?>(context) {
        val bundle = args
        @Nullable
        override fun loadInBackground(): ProductDetails? {
            var productDetails: ProductDetails? = null
            val product: NewProduct = NewProduct(bundle?.get("title") as String, bundle.get("price") as String, bundle.get("desc") as String)
            val call = HttpService
                .getProductsApi()
                ?.addProduct( HttpRequests.getToken(), product)
            try {
                val res: Response<ProductDetails?>? = call?.execute()
                productDetails = res?.body()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return productDetails
        }

        override fun onStartLoading() {
            super.onStartLoading()
            forceLoad()
        }
    }


}