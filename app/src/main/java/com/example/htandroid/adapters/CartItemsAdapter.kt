package com.example.htandroid.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.htandroid.R
import com.example.htandroid.data.model.CartItem
import com.squareup.picasso.Picasso

class CartItemsAdapter(cart: List<CartItem>, context: Context) :
    RecyclerView.Adapter<CartItemsAdapter.CartViewHolder>() {

    private val cartList: List<CartItem> = cart


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val view: View =
            LayoutInflater.from(parent.getContext())
                .inflate(
                    R.layout.cart_item,
                    parent,
                    false
                )
        return CartViewHolder(view)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        val cartItem: CartItem = cartList.get(position)
        holder.cartItemText.text = cartItem.getTitle()
        Picasso.get().load(cartItem.getPicture()).into( holder.cartItemImage)
    }

    override fun getItemCount(): Int {
        return cartList.size
    }

    class CartViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cartItemText: TextView
        val cartItemImage: ImageView

        init {
            this.cartItemText = view.findViewById(R.id.cartItemText)
            this.cartItemImage = view.findViewById(R.id.cartItemImage)
        }
    }

}