package com.example.htandroid.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.htandroid.ProductDetailsActivity
import com.example.htandroid.R
import com.example.htandroid.constants.NoImage
import com.example.htandroid.constants.Params
import com.example.htandroid.data.model.ProductInList
import com.squareup.picasso.Picasso


class ProductsListAdapter(products: List<ProductInList>, context: Context) :
    RecyclerView.Adapter<ProductsListAdapter.ProductViewHolder>() {
    private val productsList: List<ProductInList> = products
    private val context: Context = context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view: View =
            LayoutInflater.from(parent.getContext())
                .inflate(
                    R.layout.product_preview,
                    parent,
                    false
                )
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product: ProductInList = productsList.get(position)
        holder.titleText.text = product.getTitle()
        holder.descriptionText.text = product.getDescription()
        holder.priceText.text = "$" + product.getPrice().toString()
        Picasso.get().load(product.getPicture() ?: NoImage.getImageUrl()).into( holder.productImage)

        holder.productCard.setOnClickListener {
            context.startActivity(Intent(context, ProductDetailsActivity::class.java).apply {
                putExtra(Params.PRODUCT_ID, product.getId())
            })
        }
    }

    override fun getItemCount(): Int {
        return productsList.size
    }

    class ProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titleText: TextView
        val descriptionText: TextView
        val priceText: TextView
        val productImage: ImageView
        val productCard: CardView

        init {
            this.titleText = view.findViewById(R.id.titleText)
            this.descriptionText = view.findViewById(R.id.descriptionText)
            this.priceText = view.findViewById(R.id.priceText)
            this.productImage = view.findViewById(R.id.productImage)
            this.productCard = view.findViewById(R.id.productCard)
        }
    }
}