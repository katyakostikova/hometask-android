package com.example.htandroid

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.loader.app.LoaderManager
import androidx.loader.content.AsyncTaskLoader
import androidx.loader.content.Loader
import com.example.htandroid.constants.HttpRequests
import com.example.htandroid.constants.NoImage
import com.example.htandroid.constants.Params
import com.example.htandroid.data.HttpService
import com.example.htandroid.data.model.CartItem
import com.example.htandroid.data.model.ProductDetails
import com.example.htandroid.data.store.Cart
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product_details.*
import retrofit2.Response
import java.io.IOException

class ProductDetailsActivity : AppCompatActivity(), LoaderManager.LoaderCallbacks<Any> {

    var productId = 0;
    var productDetails: ProductDetails? = null
    var currentPictureId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)

        productId = intent.getIntExtra(Params.PRODUCT_ID, 0)

        LoaderManager.getInstance(this).initLoader(1, null, this);

        deleteButton.setOnClickListener {
            LoaderManager.getInstance(this).initLoader(2, null, this);
        }
    }

    fun setProductDetails() {
        titleDetailsText.text = productDetails?.getTitle()
        priceDetailsText.text = "$" + productDetails?.getPrice().toString()
        descriptionDetailsText.text = productDetails?.getDescription()
        if (productDetails?.getPictures()?.isEmpty() == false) {
            Picasso.get().load(productDetails?.getPictures()?.get(currentPictureId)?.getUrl())
                .into(productDetailsImage)
        } else {
            Picasso.get().load(NoImage.getImageUrl())
                .into(productDetailsImage)
        }
        Picasso.get().load(productDetails?.getSeller()?.getAvatar() ?: NoImage.getImageUrl())
            .into(sellerImage)
        nameText.text = productDetails?.getSeller()?.getName()
        numberText.text = productDetails?.getSeller()?.getPhoneNumber()

        if (productDetails?.getSeller()?.getEmail() == HttpRequests.getCurrentUserEmail()) {
            deleteButton.visibility = View.VISIBLE
        }

        callButton.setOnClickListener {
            callSeller()
        }

        imageNextButton.setOnClickListener {
            if (productDetails?.getPictures()
                    ?.isEmpty() == false && currentPictureId + 1 < productDetails?.getPictures()?.size as Int
            ) {
                currentPictureId++
                Picasso.get().load(productDetails?.getPictures()?.get(currentPictureId)?.getUrl())
                    .into(productDetailsImage)
            }
        }

        imageBackButton.setOnClickListener {
            if (productDetails?.getPictures()?.isEmpty() == false && currentPictureId > 0) {
                currentPictureId--
                Picasso.get().load(productDetails?.getPictures()?.get(currentPictureId)?.getUrl())
                    .into(productDetailsImage)
            }
        }

        addToCartButton.setOnClickListener {
            var image = NoImage.getImageUrl()
            if (productDetails?.getPictures()?.isEmpty() == false) {
               image = productDetails?.getPictures()?.get(0)?.getUrl().toString()
            }
            Cart.addToCart(CartItem(productDetails?.getTitle(), image))
            Toast.makeText(this, "Added to cart successfully", Toast.LENGTH_SHORT).show()
        }
    }

    fun callSeller() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE)

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this, arrayOf(android.Manifest.permission.CALL_PHONE),
                123
            )
        } else {
            startActivity(
                Intent(Intent.ACTION_CALL).setData(
                    Uri.parse(
                        "tel:" + productDetails?.getSeller()?.getPhoneNumber()
                    )
                )
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            123 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callSeller()
            } else {
                Log.d("TAG", "Call Permission Not Granted")
            }
            else -> {
            }
        }
    }


    override fun onCreateLoader(id: Int, args: Bundle?): ProductDetailsLoader {
        return ProductDetailsLoader(applicationContext, productId, id)
    }

    override fun onLoadFinished(loader: Loader<Any>, data: Any?) {
        if (data != null) {
            if (data is Int) {
                Toast.makeText(this, "Product has been deleted", Toast.LENGTH_LONG).show()
                finish()
            } else {
                this.productDetails = data as ProductDetails
                setProductDetails()
            }

        } else {
            Toast.makeText(this, "Server error", Toast.LENGTH_LONG).show()
        }
    }

    override fun onLoaderReset(loader: Loader<Any>) {
    }


    open class ProductDetailsLoader(
        context: Context,
        private val productId: Int,
        private val loaderId: Int
    ) :
        AsyncTaskLoader<Any>(context) {
        @Nullable
        override fun loadInBackground(): Any? {
            when (loaderId) {
                1 -> {
                    var productDetails: ProductDetails? = null
                    val call = HttpService
                        .getProductsApi()
                        ?.getProductDetails(
                            token = HttpRequests.getToken(),
                            id = productId
                        )
                    try {
                        val res: Response<ProductDetails?>? = call?.execute()
                        productDetails = res?.body()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    return productDetails as Any
                }
                2 -> {
                    val call = HttpService
                        .getProductsApi()
                        ?.deleteProduct(
                            token = HttpRequests.getToken(),
                            id = productId
                        )
                    try {
                        call?.execute()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    return productId
                }
            }
            return null
        }

        override fun onStartLoading() {
            super.onStartLoading()
            forceLoad()
        }
    }

}