package com.example.htandroid.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Seller {

    @SerializedName("name")
    @Expose
    private var name: String? = null

    @SerializedName("email")
    @Expose
    private var email:String? = null

    @SerializedName("phoneNumber")
    @Expose
    private var phoneNumber: String? = null

    @SerializedName("avatar")
    @Expose
    private var avatar: String? = null

    fun getName(): String? {
        return name
    }

    fun getEmail(): String? {
        return email
    }

    fun getPhoneNumber(): String? {
        return phoneNumber
    }

    fun getAvatar(): String? {
        return avatar
    }

}