package com.example.htandroid.data.model

class CartItem(title: String?, picture: String?) {

    private var title: String? = null
    private var picture: String? = null

    init {
        this.title = title
        this.picture = picture
    }

    fun getTitle(): String? {
        return title
    }

    fun getPicture(): String? {
        return picture
    }
}