package com.example.htandroid.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductDetails {

    @SerializedName("id")
    @Expose
    private var id = 0

    @SerializedName("title")
    @Expose
    private var title: String? = null

    @SerializedName("price")
    @Expose
    private var price = 0.0

    @SerializedName("description")
    @Expose
    private var description: String? = null

    @SerializedName("pictures")
    @Expose
    private var pictures: List<Picture>? = null

    @SerializedName("seller")
    @Expose
    private var seller: Seller? = null

    fun getId(): Int {
        return id
    }


    fun getTitle(): String? {
        return title
    }

    fun getDescription(): String? {
        return description
    }


    fun getPrice(): Double? {
        return price
    }

    fun getPictures(): List<Picture>? {
        return pictures
    }

    fun getSeller(): Seller? {
        return seller
    }

}