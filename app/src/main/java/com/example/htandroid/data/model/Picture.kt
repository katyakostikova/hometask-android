package com.example.htandroid.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Picture {
    @SerializedName("id")
    @Expose
    private var id = 0

    @SerializedName("url")
    @Expose
    private var url: String? = null

    fun getId(): Int {
        return id
    }

    fun getUrl(): String? {
        return url
    }
}