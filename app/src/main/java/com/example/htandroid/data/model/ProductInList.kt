package com.example.htandroid.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class ProductInList {
    @SerializedName("id")
    @Expose
    private var id = 0

    @SerializedName("title")
    @Expose
    private var title: String? = null

    @SerializedName("price")
    @Expose
    private var price = 0.0

    @SerializedName("description")
    @Expose
    private var description: String? = null

    @SerializedName("picture")
    @Expose
    private var picture: String? = null

    fun getId(): Int {
        return id
    }

    fun getPrice(): Double {
        return price
    }


    fun getTitle(): String? {
        return title
    }

    fun getDescription(): String? {
        return description
    }


    fun getPicture(): String? {
        return picture
    }

}