package com.example.htandroid.data

import com.example.htandroid.data.model.NewProduct
import com.example.htandroid.data.model.ProductDetails
import com.example.htandroid.data.model.ProductInList
import retrofit2.Call
import retrofit2.http.*

interface IProductsApi {

    @GET("api/Products")
    fun getProducts(
        @Header("Authorization") token: String,
        @Query("page") page: String,
        @Query("perPage") perPage: String,
        @Query("filter") filter: String
    ): Call<List<ProductInList>?>?


    @GET("api/Products/{id}")
    fun getProductDetails(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Call<ProductDetails>?

    @POST("api/Products")
    fun addProduct(
        @Header("Authorization") token: String,
        @Body data: NewProduct
    ): Call<ProductDetails>?

    @DELETE("api/Products/{id}")
    fun deleteProduct(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Call<ProductDetails>?
}