package com.example.htandroid.data

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object HttpService {
    private var service: HttpService? = null
    private val API_PATH = "https://bsa-marketplace.azurewebsites.net/"
    private var retrofit = Retrofit.Builder()
        .baseUrl(API_PATH)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun getProductsApi(): IProductsApi? {
        return retrofit?.create(IProductsApi::class.java)
    }
}