package com.example.htandroid.data.store

import com.example.htandroid.data.model.CartItem

object Cart {
    private val usersCart: MutableList<CartItem> = mutableListOf()

    fun getCartItems(): List<CartItem> {
        return usersCart
    }

    fun addToCart(cartItem: CartItem) {
        usersCart.add(cartItem)
    }

    fun clearCart() {
        usersCart.clear()
    }
}